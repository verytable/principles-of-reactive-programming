package simulations

import math.random
import scala.util.Random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  // parameters of simulation
  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8
    val infectedRate: Double = 0.01
    val transmissibilityRate: Double = 0.4
    val restTime: Int = 5
    val incubationTime: Int = 6
    val diseaseTime: Int = 14
    val immunityTime: Int = 16
    val recoveryTime: Int = 18
    val deathProbability = 0.25
  }

  import SimConfig._

  val persons: List[Person] = ((1 to population) map (new Person(_))).toList
  
  infect()
  for (person <- persons) person.move()
  
  def infect(): Unit = {
    var curInfected = 0
    val nInfected = infectedRate * population
    do {
      var id = randomBelow(population)
      if (!persons(id).infected) {
        persons(id).infected = true
        addDiseaseActions(persons(id))
        curInfected += 1
      }
    } while (curInfected < nInfected)
  }
  
  def addDiseaseActions(person: Person): Unit = {
    afterDelay(incubationTime) {
      person.sick = true
    }
    afterDelay(diseaseTime) {
      if (random < deathProbability) {
        person.dead = true
      }
    }
    afterDelay(immunityTime) {
      person.sick = false
      person.dead = false
      person.immune = true
    }
    afterDelay(recoveryTime) {
      person.infected = false
      person.sick = false
      person.dead = false
      person.immune = false
    }
  }
  
  class Person (val id: Int) {
    var infected = false
    var sick = false
    var immune = false
    var dead = false

    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)
    
    def getNeighbourRooms() = {
      val left = if (col == 0) (row, roomColumns - 1) else (row, col - 1)
      val right = if (col == roomColumns - 1) (row, 0) else (row, col + 1)
      val up = if (row == roomRows - 1) (0, col) else (row + 1, col)
      val down = if (row == 0) (roomRows - 1, col) else (row - 1, col)
      scala.collection.mutable.Set(left, right, up, down)
    }
    
    def chooseRoom(): (Int, Int) = {
      val visiblySafeRooms = getNeighbourRooms()
      for (person <- persons) {
        if (visiblySafeRooms.contains((person.row, person.col))
            && (person.sick || person.dead)) {
          visiblySafeRooms -= ((person.row, person.col))
        }
      }
      if (visiblySafeRooms.size == 0) {
        (row, col)
      } else {
        Random.shuffle(visiblySafeRooms.toList).head
      }
    }
    
    def hasInfectedPerson(room: (Int, Int)): Boolean = {
      for (person <- persons) {
        if ((person.row, person.col) == room && person.infected) {
          return true
        }
      }
      false
    }
    
    def move(): Unit = {
      val delay = randomBelow(restTime) + 1
      afterDelay(delay) {
        if (!dead) {
          val room = chooseRoom()
          if (room != (row, col)) {
            if (!infected && hasInfectedPerson(room)) {
              if (random < transmissibilityRate) {
                infected = true
                addDiseaseActions(this)
              }
            }
            row = room._1
            col = room._2
          } 
        }
      }
      afterDelay(delay + 1) {
        move()
      }
    }
  }
}
