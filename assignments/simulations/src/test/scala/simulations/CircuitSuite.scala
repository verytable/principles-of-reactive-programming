package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }
  
  test("orGate example") {
    val in1, in2, out = new Wire
    orGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "or 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === true, "or 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "or 3")
  }
  
  test("orGate2 example") {
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "or2 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === true, "or2 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "or2 3")
  }
  
  test("demux example 1") {
    val in, out0 = new Wire
    val out = List(out0)
    demux(in, Nil, out)
    in.setSignal(false)
    run
    
    assert(out(0).getSignal === false, "demux 1.1")
    
    in.setSignal(true)
    run
    
    assert(out(0).getSignal === true, "demux 1.2")
  }
  
  test("demux example 2") {
    val in, out0, out1, c0 = new Wire
    val out = List(out0, out1)
    val c = List(c0)
    demux(in, c, out)
    in.setSignal(false)
    c(0).setSignal(false)
    run
    
    assert(out(0).getSignal === false, "demux 2.1")
    assert(out(1).getSignal === false, "demux 2.2")
    
    c(0).setSignal(true)
    run
    
    assert(out(0).getSignal === false, "demux 2.3")
    assert(out(1).getSignal === false, "demux 2.4")
    
    in.setSignal(true)
    run
    
    assert(out(0).getSignal === true, "demux 2.5")
    assert(out(1).getSignal === false, "demux 2.6")
    
    c(0).setSignal(false)
    run
    
    assert(out(0).getSignal === false, "demux 2.7")
    assert(out(1).getSignal === true, "demux 2.8")
  }

}
