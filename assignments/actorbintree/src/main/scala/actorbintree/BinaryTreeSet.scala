/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue
import scala.util.Random

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case op: Operation => root ! op
    case GC =>
      val newRoot = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true), s"newRoot${Random.nextInt(100)}")
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot))
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case op: Operation =>
      pendingQueue = pendingQueue.enqueue(op)
    case CopyFinished =>
      for(op <- pendingQueue) { newRoot ! op }
      pendingQueue = Queue.empty[Operation]
      root ! PoisonPill
      root = newRoot
      context.become(normal)
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor with ActorLogging {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    
    case Insert(requester, id, e) =>
      if(e < elem) {
        if (subtrees.contains(Left)) {
          subtrees(Left) ! Insert(requester, id, e)
        } else {
          subtrees += Left -> context.actorOf(BinaryTreeNode.props(e, initiallyRemoved = false), s"____${e}____")
          requester ! OperationFinished(id)
        }
      } else if(elem < e) {
        if (subtrees.contains(Right)) {
          subtrees(Right) ! Insert(requester, id, e)
        } else {
          subtrees += Right -> context.actorOf(BinaryTreeNode.props(e, initiallyRemoved = false), s"____${e}____")
          requester ! OperationFinished(id)
        }
      } else {
        removed = false
        requester ! OperationFinished(id)
      }
      
    case Contains(requester, id, e) =>
      if(e < elem) {
        if (subtrees.contains(Left)) {
          subtrees(Left) ! Contains(requester, id, e)
        } else {
          requester ! ContainsResult(id, false)
        }
      } else if(elem < e) {
        if (subtrees.contains(Right)) {
          subtrees(Right) ! Contains(requester, id, e)
        } else {
          requester ! ContainsResult(id, false)
        }
      } else {
        requester ! ContainsResult(id, !removed)
      }
      
    case Remove(requester, id, e) =>
      if(e < elem) {
        if(subtrees.contains(Left)) {
          subtrees(Left) ! Remove(requester, id, e)
        } else {
          requester ! OperationFinished(id)
        }
      } else if(elem < e) {
        if(subtrees.contains(Right)) {
          subtrees(Right) ! Remove(requester, id, e)
        } else {
          requester ! OperationFinished(id)
        }
      } else {
        removed = true
        requester ! OperationFinished(id)
      }
      
    case CopyTo(treeNode) =>
      val children = subtrees.values.toSet
      if(children.isEmpty && removed) {
        sender ! CopyFinished
      } else {
        for(tree <- children) { tree ! CopyTo(treeNode) }
        if(!removed) {
          treeNode ! Insert(self, elem, elem)
          context.become(copying(children, false))
        } else {
          context.become(copying(children, true))
        }
      }
      
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case OperationFinished(id) =>
      if(expected.isEmpty) {
        context.parent ! CopyFinished
      } else {
        context.become(copying(expected, true))
      }
    case CopyFinished =>
      val updatedExpected = expected - sender
      if(insertConfirmed && updatedExpected.isEmpty) {
        context.parent ! CopyFinished
      } else {
        context.become(copying(updatedExpected, insertConfirmed))
      }
  }

}
