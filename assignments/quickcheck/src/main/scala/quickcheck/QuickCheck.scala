package quickcheck

import common._
import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import scala.collection.immutable.Range.Partial

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }
  
  property("min2") = forAll { (a: Int, b: Int) =>
    val h = insert(b, insert(a, empty))
    findMin(h) == Math.min(a, b)
  }
  
  property("min3") = forAll { (h: H) =>
    if (!isEmpty(h)) {
      val m1 = findMin(h)
      val h1 = deleteMin(h)
      if (!isEmpty(h1)) {
        m1 <= findMin(h1)
      } else {
        true
      }
    } else {
      true
    }
    
  }
  
  property("empty1") = forAll {a: Int =>
    val h = deleteMin(insert(a, empty))
    isEmpty(h)
  }
  
  def getHeapElems(h: H): List[Int] = isEmpty(h) match {
    case true => List()
    case false => findMin(h) :: getHeapElems(deleteMin(h))
  }
  
//  property("gen0") = forAll { (elems: List[Int]) =>
//    def ins(elem: Int, heap: H): H = {
//      insert(elem, heap)
//    }
//    val h = elems foldLeft(empty)()
//    elems.toSet == (getHeapElems(h).toSet)
//  }
  
  property("gen0") = forAll {elems: List[Int] =>
    var h = empty
    for (elem <- elems) {
      h = insert(elem, h)
    }
    elems.toSet.equals(getHeapElems(h).toSet)
  }
  
  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h))==m
  }
  
  property("gen2") = forAll { (h: H) =>
    val elems = getHeapElems(h)
    elems == elems.sorted
  }
  
  property("gen3") = forAll { (h1: H, h2: H) =>
    val m1 = if (isEmpty(h1)) Int.MaxValue else findMin(h1)
    val m2 = if (isEmpty(h2)) Int.MaxValue else findMin(h2)
    val h = meld(h1, h2)
    val m = if (isEmpty(h)) Int.MaxValue else findMin(h)
    (m == Math.min(m1, m2))
  }
  
  def heapSize(h: H): Int = isEmpty(h) match {
    case true => 0
    case false => 1 + heapSize(deleteMin(h))
  }
  
  property("gen4") = forAll { (h1: H, h2: H) =>
    heapSize(h1) + heapSize(h2) == heapSize(meld(h1, h2))
  }
  
  property("gen5") = forAll { (h: H, elem: Int) =>
    heapSize(insert(elem, h)) == 1 + heapSize(h)
  }
  
  property("gen6") = forAll { (h: H, elem: Int) =>
    heapSize(h) == heapSize(deleteMin(insert(elem, h)))
  }
  
  property("gen7") = forAll { (h: H, elem: Int) =>
    val h1 = insert(elem, h)
    heapSize(h1) == 1 + heapSize(deleteMin(h1))
  }

  def genEmptyHeap: Gen[H] = empty
  
  lazy val genNonEmptyHeap: Gen[H] = for {
    elem <- arbitrary[Int]
    heap <- oneOf(empty, genNonEmptyHeap)
  } yield insert(elem, heap)
  
  lazy val genHeap: Gen[H] = for {
    isEmpty <- arbitrary[Boolean]
    heap <- if (isEmpty) genEmptyHeap else genNonEmptyHeap
  } yield heap

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
