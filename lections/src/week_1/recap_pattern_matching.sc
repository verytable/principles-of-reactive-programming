import week_1._

object recap_pattern_match {
  //due to intellij bugs "->" in Maps was replaced by "()"
  val data = JObj(Map(
    ("firstName", JStr("John")),
    ("lastName", JStr("Smith")),
    ("address", JObj(Map(
      ("streetAddress", JStr("21 2nd Street")),
      ("state", JStr("NY")),
      ("postalCode", JNum(10021))
    ))),
    ("phoneNumbers", JSeq(List(
      JObj(Map(
        ("type", JStr("home")),
        ("number", JStr("212 555-1234"))
      )),
      JObj(Map(
        ("type", JStr("fax")),
        ("number", JStr("646 555-4567"))
      )))))))

  def show(json: JSON): String = json match {
    case JSeq(elems) =>
      "[" + (elems map show mkString ", ") + "]"
    case JObj(bindings) =>
      val assocs = bindings map {
        case (key, value) => "\"" + key + "\": " + show(value)
      }
      "{" + (assocs mkString ", ") + "}"
    case JNum(num) => num.toString
    case JStr(str) => '\"' + str + '\"'
    case JBool(b)  => b.toString
    case JNull     => "null"
  }

  show(data)

  val dataList: List[JSON] = List(data)

  for {
    JObj(bindings) <- dataList
    JSeq(phones) = bindings("phoneNumbers")
    JObj(phone) <- phones
    JStr(digits) = phone("number")
    if digits startsWith "212"
  } yield (bindings("firstName"), bindings("lastName"))
}