object test {

  val f: String => String = {case "ping" => "pong"}
  f("ping")
  //f("abc") //leads to scala.MatchError

  val g: PartialFunction[String, String] = { case "ping" => "pong" }
  g.isDefinedAt("ping")
  g.isDefinedAt("abc")

  val f1: PartialFunction[List[Int], String] = {
    case Nil => "one"
    case x :: y :: rest => "two"
  }

  f1.isDefinedAt(List(1, 2, 3))

  val f2: PartialFunction[List[Int], String] = {
    case Nil => "one"
    case x :: rest =>
      rest match {
        case Nil => "two"
      }
  }

  f2.isDefinedAt(List(1, 2, 3))
  //f2(List(1, 2, 3)) //leads to scala.MatchError although it is defined
}