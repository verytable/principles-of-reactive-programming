package week_1

/**
 * Created by arseny on 15.07.14.
 */
trait Tree {
}

case class Inner(left: Tree, right: Tree) extends Tree

case class Leaf(x: Int) extends Tree