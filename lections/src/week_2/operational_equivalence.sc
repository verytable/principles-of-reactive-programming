import week_2.BankAccount

object account {
  val x = new BankAccount
  val y = new BankAccount
  /*
  * Are two statements above operationally equivalent?
  * No, because results of next operations differ for (x, y) and (x, x)
  * */
  x deposit 30
  y withdraw 20
}