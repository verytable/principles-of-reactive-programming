package week_2

import scala.annotation.tailrec

/**
 * Created by arseny on 22.07.14.
 */
/*
* A concrete simulation happens inside an object that inherits from the trait
* Simulation
* */
abstract class Simulation {

  /*
  * An action is a function that doesn't take any parameters and which returns
  * Unit
  * */
  type Action = () => Unit

  /*
  * Event is an action and the time when it must be performed
  * */
  case class Event(time: Int, action: Action)

  /*
  * curtime contains current simulation time
  * */
  private var curtime = 0

  /*
  * currentTime returns the current simulated time in the form of an integer
  * */
  def currentTime: Int = curtime

  /*
  * agenda is a list of simulated events sorted in such a way that the actions
  * to be performed first are in the beginning
  * */
  private var agenda: List[Event] = List()

  /*
  * inserts an event to the agenda keeping it sorted
  * */
  private def insert(ag: List[Event], item: Event): List[Event] = ag match {
    case first :: rest if first.time <= item.time => first :: insert(rest, item)
    case _ => item :: ag
  }

  /*
  * afterDelay registers an action to perform after a certain delay (relative to
  * current time, currentTime)
  * block is some statements to be performed as an action (delay time units
  * after currentTime)
  * */
  def afterDelay(delay: Int)(block: => Unit): Unit = {
    val item = Event(currentTime + delay, () => block)
    insert(agenda, item)
  }

  /*
  * run performs the simulation until there are no more actions waiting
  * */
  def run(): Unit = {
    afterDelay(0) {
      println(s"*** simulation started, time = $currentTime ***")
    }
    loop()
  }

  /*
  * The event handling loop removes successive elements from the agenda and
  * performs the associated actions
  * */
  @tailrec
  private def loop(): Unit = agenda match {
    case first :: rest =>
      agenda = rest
      curtime = first.time
      first.action()
      loop()
    case Nil =>
  }
}
