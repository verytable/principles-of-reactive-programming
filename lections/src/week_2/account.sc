import week_2.BankAccount

object account {
  val acct = new BankAccount
  acct deposit 50
  acct withdraw 20
  acct withdraw 20
  //acct withdraw 15 //leads to java.lang Error
}