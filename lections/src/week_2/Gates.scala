package week_2

/**
 * Created by arseny on 22.07.14.
 */
abstract class Gates extends Simulation {

  def InverterDelay: Int
  def AndGateDelay: Int
  def OrGateDelay: Int

  class Wire {

    /*
    * The value of the current signal
    * */
    private var sigVal = false

    /*
    * Currently attached actions to be performed when signal changes
    * */
    private var actions: List[Action] = List()

    /*
    * Returns the current value of a signal transported by the wire
    * */
    def getSignal = sigVal

    /*
    * Modifies the value of the signal transported by the wire
    * */
    def setSignal(s: Boolean): Unit =
      if (s != sigVal) {
        sigVal = s
        actions foreach (_()) // equivalently for (action <- actions) action()
      }

    /*
    * Attaches the specified procedure to the actions of the wire. All of the
    * attached actions are executed at each change of the transported signal
    * */
    def addAction(action: Action) = {
      actions = action :: actions
      action()
    }
  }

  def inverter(input: Wire, output: Wire): Unit = {
    def invertAction(): Unit = {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) {
        output setSignal !inputSig
      }
    }
    input addAction invertAction
  }

  def andGate(input1: Wire, input2: Wire, output: Wire): Unit = {
    def andAction(): Unit = {
      val input1Sig = input1.getSignal
      val input2Sig = input2.getSignal
      afterDelay(AndGateDelay) {
        output setSignal (input1Sig & input2Sig)
      }
    }
    input1 addAction andAction
    input2 addAction andAction
  }

  def orGate(input1: Wire, input2: Wire, output: Wire): Unit = {
    def orAction(): Unit = {
      val input1Sig = input1.getSignal
      val input2Sig = input2.getSignal
      afterDelay(OrGateDelay) {
        output setSignal (input1Sig | input2Sig)
      }
    }
    input1 addAction orAction
    input2 addAction orAction
  }

  /*
  * An alternative version of the OR-gate, defined in terms of AND and INV
  * */
  def ofGateAlt(input1: Wire, input2: Wire, output: Wire): Unit = {
    val notInput1, notInput2, notOutput = new Wire
    inverter(input1, notInput1)
    inverter(input2, notInput2)
    andGate(notInput1, notInput2, notOutput)
    inverter(notOutput, output)
  }

  /*
  * probe is the function to examine the changes of the signal on the wire
  * */
  def probe(name: String, wire: Wire): Unit = {
    def probeAction(): Unit = {
      println(s"$name $currentTime new-value = ${wire.getSignal}")
    }
    wire addAction probeAction
  }
}
