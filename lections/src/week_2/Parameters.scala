package week_2

/**
 * Created by arseny on 22.07.14.
 */
trait Parameters {
  def InverterDelay = 2
  def AndGateDelay = 3
  def OrGateDelay = 5
}
