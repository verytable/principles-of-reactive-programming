object loops {
  // built-in while loop
  def power(x: Double, exp: Int): Double = {
    var r = 1.0
    var i = exp
    while (i > 0) {r = r * x; i = i - 1}
    r
  }

  power(2, 5)

  // define WHILE using a function
  def WHILE(condition: => Boolean)(command: => Unit): Unit = {
    if (condition) {
      command
      WHILE(condition)(command)
    }
    else ()
  }

  var i: Int = 0

  def command(): Unit = {
    println(i)
    i = i + 1
  }

  WHILE(i < 3)(command())

  // define REPEAT using a function
  def REPEAT(condition: => Boolean)(command: => Unit): Unit = {
    command
    if (condition) {
      REPEAT(condition)(command)
    }
    else ()
  }

  REPEAT(i < 6)(command())

  i = 3

  // built-in do-while loop
  do {
    command()
  } while (i < 6)

  // built-in for loop
  for (i <- 0 until 3) print(i + " ")
  for (i <- 1 until 3; j <- "abc") println(i + " " + j)
}