package week_5.BankAccount

import akka.actor.Actor
import akka.event.LoggingReceive

/**
 * Created by arseny on 17.08.14.
 */
object BankAccount {
  case class Deposit(amount: BigInt) {
    require(amount > 0)
  }
  case class Withdraw(amount: BigInt) {
    require(amount > 0)
  }
  case object Done
  case object Failed
}

class BankAccount extends Actor {
  import week_5.BankAccount.BankAccount._
  var balance = BigInt(0)
  def receive = LoggingReceive {
    case Deposit(amount) =>
      balance += amount
      sender ! Done
    case Withdraw(amount) if(amount <= balance) =>
      balance -= amount
      sender ! Done
    case _ => sender ! Failed
  }
}
