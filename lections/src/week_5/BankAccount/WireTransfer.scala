package week_5.BankAccount

import akka.actor.{Actor, ActorRef}
import akka.event.LoggingReceive

/**
 * Created by arseny on 17.08.14.
 */
object WireTransfer {
  case class Transfer(from: ActorRef, to: ActorRef, amount: BigInt) {
    require(amount > 0)
  }
  case object Done
  case object Failed
}

class WireTransfer extends Actor {
  import week_5.BankAccount.WireTransfer._

  def receive = LoggingReceive {
    case Transfer(from, to, amount) =>
      from ! BankAccount.BankAccount.Withdraw(amount)
      context.become(awaitFrom(to, amount, sender))
  }

  def awaitFrom(to: ActorRef, amount: BigInt, customer: ActorRef): Receive = LoggingReceive {
    case BankAccount.BankAccount.Done =>
      to ! BankAccount.BankAccount.Deposit(amount)
      context.become(awaitTo(customer))
    case BankAccount.BankAccount.Failed =>
      customer ! Failed
      context.stop(self)
  }

  def awaitTo(customer: ActorRef): Receive = LoggingReceive {
    case BankAccount.BankAccount.Done =>
      customer ! Done
      context.stop(self)
  }
}
