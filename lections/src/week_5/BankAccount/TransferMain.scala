package week_5.BankAccount

import akka.actor.{Actor, Props}
import akka.event.LoggingReceive

/**
 * Created by arseny on 18.08.14.
 */
class TransferMain extends Actor {
  val accountA = context.actorOf(Props[BankAccount], "accountA")
  val accountB = context.actorOf(Props[BankAccount], "accountB")

  accountA ! BankAccount.BankAccount.Deposit(100)

  def receive = LoggingReceive {
    case BankAccount.BankAccount.Done => transfer(180)
  }

  def transfer(amount: BigInt): Unit = {
    val transaction = context.actorOf(Props[WireTransfer], "transfer")
    transaction ! WireTransfer.Transfer(accountA, accountB, amount)
    context.become(LoggingReceive {
      case WireTransfer.Done =>
        println("success")
        context.stop(self)
      case WireTransfer.Failed =>
        println("failed")
        context.stop(self)
    })
  }
}
