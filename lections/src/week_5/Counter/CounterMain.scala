package week_5.Counter

import akka.actor.{Actor, Props}

/**
 * Created by arseny on 17.08.14.
 */
class CounterMain extends Actor {
  val counter = context.actorOf(Props[Counter], "counter")

  counter ! "increment"
  counter ! "increment"
  counter ! "increment"
  counter ! "get"

  def receive = {
    case count: Int =>
      println(s"count was $count")
      context.stop(self)
  }
}
