package week_5.Counter

import akka.actor.Actor

/**
 * Created by arseny on 17.08.14.
 */
class Counter extends Actor {
  def counter(n: Int): Receive = {
    case "increment" => context.become(counter(n + 1))
    case "get"       => sender ! n
  }
  def receive = counter(0)
}
