package week_5.TestingExample

import akka.actor.Actor

/**
 * Created by arseny on 18.08.14.
 */
class Toggle extends Actor {
  def happy: Receive = {
    case "How are you?" =>
      sender ! "happy"
      context become sad
  }

  def sad: Receive = {
    case "How are you?" =>
      sender ! "sad"
      context become happy
  }

  def receive = happy
}
