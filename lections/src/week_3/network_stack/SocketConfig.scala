package week_3.network_stack

import akka.actor.ActorSystem
import akka.serialization.SerializationExtension
import scala.collection.immutable.Queue

/**
 * Created by arseny on 28.07.14.
 */
object SocketConfig {
  val memory = Queue[EMailMessage](
    EMailMessage(from = "Eric", to = "Roland"),
    EMailMessage(from = "Martin", to = "Eric"),
    EMailMessage(from = "Roland", to = "Martin")
  )
  val system = ActorSystem("socket")

  val serialization = SerializationExtension(system)
}

case class EMailMessage(from: String, to: String)

class NetworkStackException(msg: String) extends RuntimeException(msg)