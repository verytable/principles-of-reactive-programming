package week_3.network_stack

import week_3.network_stack.SocketConfig._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by arseny on 28.07.14.
 */
trait Socket {

  def readFromMemory(): Future[Array[Byte]] = Future {
    val email = memory.dequeue
    val serializer = serialization.findSerializerFor(email)
    serializer.toBinary(email)
  }

  def sendToEurope(packet: Array[Byte]): Future[Array[Byte]] = Future {
    println("sendToEurope")
    packet
  }
}
