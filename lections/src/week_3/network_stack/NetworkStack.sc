import week_3.network_stack._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object NetworkStack {
  val socket = new Socket {}
  val packet = socket.readFromMemory()
  val confirmation: Future[Array[Byte]] = {
    packet.flatMap(p => {
      socket.sendToEurope(p)
    })
  }
  //val c = Await.result(confirmation, 1000 millis)
}