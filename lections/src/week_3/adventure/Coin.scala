package week_3.adventure

import week_3.adventure.AdventureConfig._

/**
 * Created by arseny on 28.07.14.
 */
trait Coin {
  def value: Int
}

case object Gold extends Coin {
  def value = goldValue
}

case object Silver extends Coin {
  def value = silverValue
}

case object Bronze extends Coin {
  def value = bronzeValue
}
