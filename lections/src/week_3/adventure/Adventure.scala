package week_3.adventure

import week_3.adventure.AdventureConfig._
import scala.util.Try

/**
 * Created by arseny on 28.07.14.
 */
trait Adventure {

  var eatenByMonster: Boolean = false

  def collectCoins(): Try[List[Coin]] = Try {
    if (eatenByMonster) {
      throw new GameOverException("Oops")
    }
    List(Gold, Gold, Silver)
  }

  def buyTreasure(coins: List[Coin]): Try[Treasure] = Try {
    if (coins.map(_.value).sum < treasureCost) {
      throw new GameOverException("Nice try!")
    }
    Diamond
  }
}
