import week_3.adventure.{Treasure, Adventure}
import scala.util.{Try, Failure, Success}

object Game {

  val adventure = new Adventure {}

  val coins = adventure.collectCoins()

  /*
  * Using pattern matching
  * */
  val treasure1 = coins match {
    case Success(cs) => adventure.buyTreasure(cs)
    case failure @ Failure(t) => failure
  }

  /*
  * Using flatMap
  * */
  val treasure2: Try[Treasure] = {
    adventure.collectCoins().flatMap(coins => {
      adventure.buyTreasure(coins)
    })
  }

  /*
  * Using comprehension syntax
  * */
  val treasure3: Try[Treasure] = for {
    coins <- adventure.collectCoins()
    treasure <- adventure.buyTreasure(coins)
  } yield treasure
}