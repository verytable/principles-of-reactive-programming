package week_3.adventure

/**
 * Created by arseny on 28.07.14.
 */
object AdventureConfig {
  val goldValue: Int = 5
  val silverValue: Int = 3
  val bronzeValue: Int = 1
  val treasureCost = 7
}

class GameOverException(msg: String) extends RuntimeException(msg)